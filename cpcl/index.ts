/*
 * @Date: 2023-04-26 21:41:42
 * @LastEditors: Xavier 34981520+Xavier9896@users.noreply.github.com
 * @LastEditTime: 2023-04-29 10:01:19
 * @FilePath: /cpcl/index.js
 */

import panels from './panels'
import { changeJson } from './toCPCL'

const printData = [
    {
        deviceNo: '222222A22M2222B222JH',
        name: '戴尔主机',
        qrcode: 'qrcode',
        barcode: 'barcode',
    },
    {
        deviceNo: '222222A22M2222B222HW',
        name: '华为主机',
        qrcode: 'qrcode',
        barcode: 'barcode',
    },
]

export const ancwer = () => {
    console.log('panels, printData');
    return changeJson(panels, printData)
}
