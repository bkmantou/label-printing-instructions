import tsplInstruct from './instruct'

const dpi: number = 203
let command = tsplInstruct()

const dot = (pt: number) => {
    return (pt * dpi) / 72
}

const setLevel = (level: number | string) => {
    switch (level) {
        case '1':
            level = 'L'
            break;
        case '2':
            level = 'H'
            break;
        case '3':
            level = 'Q'
            break;
        default:
            level = 'M'
            break;
    }
    return level
}

export const generateInstruct = (panels: any, blueTooth: any) => {
    command.clearCommand()
    command.setSize(panels.width, panels.height) //设定卷标纸的宽度和长度
    command.setGap(2) // 定义两张卷标纸间的垂直间距距离
    command.setCls(); //清除打印机缓存
    panels.printElements.map((item: any) => {
        if (item.printElementType.type === 'text') {
            switch (item.options.textType) {
                case 'qrcode':
                case 'barcode':
                    item.printElementType.type = item.options.textType
                    break
            }
        }
    })
    panels.printElements.map((item: any) => {
        let {
            left,
            top,
            width,
            height,
            fontSize,
            field,
            title,
            hideTitle,
            textAlign,
            textContentVerticalAlign,
            fontWeight,
            lineHeight,
            testData,
            barcodeMode,
            qrCodeLevel
        } = item.options
        left = dot(left)
        top = dot(top)
        width = dot(width)
        height = dot(height)
        lineHeight = dot(lineHeight || fontSize || 9)
        // console.log(
        //     left,
        //     top,
        //     width,
        //     height,
        //     fontSize,
        //     field,
        //     title,
        //     hideTitle,
        //     textAlign,
        //     textContentVerticalAlign,
        //     fontWeight,
        //     lineHeight,
        //     testData,
        //     barcodeMode,
        //     qrCodeLevel);

        switch (item.printElementType.type) {
            case 'text':
                command.setText(left, top, 'TSS24.BF2', 1, 1, `${hideTitle ? '' : title || ''}${!hideTitle && field ? ':' : ''}${testData || ''}`)
                break;
            case 'qrcode':
                command.setQR(left, top, setLevel(qrCodeLevel), 7, 'A', testData)
                break;
            case 'barcode':
                command.setBarCode(left, top, '128', height, `${hideTitle ? 0 : 1}`, 2, 2, testData);
                break;
        }
    })
    command.setPagePrint()
    const uint8Array = new Uint8Array(command.getData());
    let opt = {
        deviceId: blueTooth.BLEInformation.deviceId,
        serviceId: blueTooth.BLEInformation.writeServiceId,
        characteristicId: blueTooth.BLEInformation.writeCharaterId,
        value: uint8Array,
        lasterSuccess: onSendSuccess,
        onceLength: 20,
    };
    sendDataToDevice(opt)
}

export const sendDataToDevice = (options: any) => {
    return new Promise((resolve, reject) => {
        let byteLength = options.value.byteLength
        console.log(byteLength, 'byteLength');
        //这里默认一次20个字节发送
        const speed = options.onceLength //20;
        if (byteLength > 0) {

            uni.writeBLECharacteristicValue({
                ...options,
                value: options.value.slice(0, byteLength > speed ? speed : byteLength),
                success(res) {
                    if (byteLength > speed) {
                        resolve(
                            sendDataToDevice({
                                ...options,
                                value: options.value.slice(speed, byteLength),
                            })
                        )
                    } else {
                        options.lasterSuccess && options.lasterSuccess()
                        resolve(null)
                    }
                },
                fail(res) {
                    reject()
                },
            })
        }
    })
}

const onSendSuccess = () => { }